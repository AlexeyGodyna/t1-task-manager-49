package ru.t1.godyna.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.ProjectDTO;
import ru.t1.godyna.tm.dto.request.project.ProjectListRequest;
import ru.t1.godyna.tm.dto.response.project.ProjectListResponse;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-list";

    @NotNull
    private final String DESCRIPTION = "Show list projects.";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        @NotNull final List<ProjectDTO> projects = response.getProjects();
        if (projects == null) throw new ProjectNotFoundException();
        @NotNull final StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (@Nullable final ProjectDTO project: projects) {
            if (project == null) continue;
            stringBuilder.append(index + ".");
            stringBuilder.append(project.getName() + " : ");
            stringBuilder.append(project.getDescription()  + " : ");
            stringBuilder.append(project.getId());
            System.out.println(stringBuilder);
            index++;
            stringBuilder.setLength(0);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

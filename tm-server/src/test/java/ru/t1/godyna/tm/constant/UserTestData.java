package ru.t1.godyna.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.service.IPropertyService;
import ru.t1.godyna.tm.dto.model.UserDTO;
import ru.t1.godyna.tm.service.PropertyService;
import ru.t1.godyna.tm.util.HashUtil;

import java.util.UUID;

public final class UserTestData {

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN = new UserDTO();

    @NotNull
    public final static String UNIT_TEST_USER_PASSWORD = "UNIT_TEST_PASSWORD";

    @NotNull
    public final static String UNIT_TEST_USER_EMAIL = "unit_test@email.test";

    @NotNull
    public final static String UNIT_TEST_INCORRECT_LOGIN = "INCORRECT_LOGIN";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    static {
        USER1.setLogin(UUID.randomUUID().toString());
        USER1.setPasswordHash(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD));
        USER1.setEmail(UNIT_TEST_USER_EMAIL);
        USER1.setFirstName("USER1");
        USER1.setMiddleName("USER1");
        USER2.setLogin(UUID.randomUUID().toString());
        USER2.setPasswordHash(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD));
        USER2.setEmail(UNIT_TEST_USER_EMAIL);
        USER2.setFirstName("USER2");
        USER2.setMiddleName("USER2");
        ADMIN.setLogin(UUID.randomUUID().toString());
        ADMIN.setPasswordHash(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD));
        ADMIN.setEmail(UNIT_TEST_USER_EMAIL);
        ADMIN.setFirstName("USER3");
        ADMIN.setMiddleName("USER3");
    }

}
